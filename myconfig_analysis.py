# coding: utf-8

############################
# This configuration file sets various parameters for running a trained model,
# that performed well on train/test set on videos
############################

# Filename and path to behavioral video (for labeling)
videofolder = '/media/beast/pierre/walking-videos/06072018'
cropping = False
videotype='.avi' #type of videos to analyze 

# ROI dimensions / bounding box (only used if cropping == True)
# x1,y1 indicates the top left corner and
# x2,y2 is the lower right corner of the croped region.

x1 = None
x2 = None
y1 = None
y2 = None

# Analysis Network parameters:

scorer = 'Pierre'
Task = 'flywalk'
date = 'May9'
trainingsFraction = 0.95  # Fraction of labeled images used for training
resnet = 50
snapshotindex = -1
shuffle = 1

#########################################################################################
## For plotting (MakingLabeledVideo.py)
#########################################################################################t
trainingsiterations = 750000  # type the number listed in .pickle file
pcutoff = 0.1  # likelihood cutoff for body part in image

# delete individual (labeled) frames after making video? (note there could be many...)
deleteindividualframes = False
alphavalue=.1 # "strength/transparency level of makers" in individual frames (Vary from 0 to 1. / not working in "MakingLabeledVideo_fast.py")
dotsize = 5
colormap='cool' #hsv'
