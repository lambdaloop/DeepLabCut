# coding: utf-8

############################
# This configuration file sets various parameters for generation of training
# set file & evalutation of results
############################

# myconfig.py:

# Step 1:
Task = 'flypose'

# Filename and path to behavioral video:
vidpath = '/home/pierre/research/tuthill/videos-summaries'
filename = '2018-09-20-pierre-C.avi'

# Number of frames to pick (set this to 0 until you found right cropping)
numframes2pick = 75


cropping = False

# ROI dimensions / bounding box (only used if cropping == True)
# x1,y1 indicates the top left corner and
# x2,y2 is the lower right corner of the croped region.

# x1 = 0
# x2 = 640
# y1 = 277
# y2 = 624

x1 = None
x2 = None
y1 = None
y2 = None


# Portion of the video to sample from in step 1. Set to 1 by default.
portion = 1

# see metadata / when bodypart not visible!
invisibleboundary = 50

# Step 2:
# bodyparts = ["hand", "Finger1", "Finger2",
#              "Joystick"]  # Exact sequence of labels as were put by
# bodyparts = ["body-coxa", "coxa-femur", "femur-tibia", "tibia-tarsus", "tarsus-end"] 

# bodyparts = ["body-coxa-left", "coxa-femur-left", "femur-tibia-left", "tibia-tarsus-left", "tarsus-end-left",
#              "body-coxa-right", "coxa-femur-right", "femur-tibia-right", "tibia-tarsus-right", "tarsus-end-right"] 

# bodyparts = ["C1", "C2", "C3", "C4", "C5", "C6",
#              "F1", "F2", "F3", "F4", "F5", "F6", "F7"] 

## naming scheme
## L1A - left (L), leg in T1 (1), joint A (A)
## A - body-coxa
## B - coxa-femur
## C - femur-tibia
## D - tibia-tarsus
## E - tarsus-end

bodyparts = ["L1A", "L1B", "L1C", "L1D", "L1E",
             "L2A", "L2B", "L2C", "L2D", "L2E"]
             # "L3A", "L3B", "L3C", "L3D", "L3E"]

# annotator in *.csv file
# Scorers = ['Pierre']  # who is labeling?
Scorers = ['TuthillLab']  # who is labeling?

# Step 3:
# date = 'Sep16'
# scorer = 'Pierre'

date = 'Sep24'
scorer = 'TuthillLab'

# Userparameters for training set. Other parameters can be set in pose_cfg.yaml
Shuffles = [1]  # Ids for shuffles, i.e. range(5) for 5 shuffles
TrainingFraction = [0.95]  # Fraction of labeled images used for training

# Which resnet to use
# (these are parameters reflected in the pose_cfg.yaml file)
resnet = 50
# trainingsiterations='1030000'

# For Evaluation/ Analyzing videos
# To evaluate model that was trained most set this to: "-1"
# To evaluate all models (training stages) set this to: "all"

snapshotindex = -1
shuffleindex = 0

