# coding: utf-8

############################
# This configuration file sets various parameters for running a trained model,
# that performed well on train/test set on videos
############################

# Filename and path to behavioral video (for labeling)
# pipeline_videos_raw = '../../flywalk-pipeline/videos-raw'
# pipeline_pose = '../../flywalk-pipeline/pose-2d'

pipeline_prefix = "/media/beast/pierre/flywalk-pipeline-new"
# prefix = "/home/pierre/research/tuthill/flywalk-pipeline/"

pipeline_videos_raw = 'videos-raw'
pipeline_pose = 'pose-2d'
pipeline_angles = 'angles'
pipeline_videos_labeled = 'videos-labeled'

cropping = False

# ROI dimensions / bounding box (only used if cropping == True)
# x1,y1 indicates the top left corner and
# x2,y2 is the lower right corner of the croped region.

x1 = None
x2 = None
y1 = None
y2 = None

# Analysis Network parameters:

scorer = 'TuthillLab'
Task = 'flypose'
date = 'Sep24'
trainingsFraction = 0.95  # Fraction of labeled images used for training
resnet = 50
snapshotindex = -1
shuffle = 1
trainingsiterations = 800000  # type the number listed in .pickle file


#########################################################################################
## For plotting (MakingLabeledVideo.py)
#########################################################################################t
pcutoff = 0.1  # likelihood cutoff for body part in image

# delete individual (labeled) frames after making video? (note there could be many...)
deleteindividualframes = False
alphavalue=.1 # "strength/transparency level of makers" in individual frames (Vary from 0 to 1. / not working in "MakingLabeledVideo_fast.py")
dotsize = 5
colormap='cool' #hsv'
